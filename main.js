﻿function Channel(channelname) {
	this.name = channelname;
	this.port = chrome.runtime.connect({name: channelname});
	this.listeners = [];
	var that = this;
	this.port.onMessage.addListener(function(reply) {
		for(var i = 0; i<that.listeners.length; i++) {
			//console.log("loop: %o <= %o =?= %o", reply.command == that.listeners[i].command, reply, that.listeners[i]);
			if(reply.command == that.listeners[i].command) {
				that.listeners[i].callback.call(that, reply);
			}
		}
	});
	
}

Channel.prototype.message = function(msg, callback) {
	this.listeners.push({'command':msg.command, 'callback':callback});
	//console.log("list: %o", this.listeners);
	this.port.postMessage(msg);
};



function main() {
	//var phonelist = $("<datalist id='phonelist'></datalist>");
	//input.attr("list", "phonelist").after(phonelist);
	var isAuthorized = false;
	var contacts = [];
	var oauthChannel = new Channel("oauth");
	var getContacts = function () {
		if(!isAuthorized) {
			return window.setTimeout(getContacts, 4*1000);
		}
		oauthChannel.message({command:'getContacts'}, function(reply) {
			var contact = reply.result;
			//console.log("got contact : %o", contact);
			if(contact === true || contact === false) return contact;
			if(contact.phones.length > 0) {
				//$("<option></option>").attr({label:contact.name, value:contact.phones[0]}).appendTo(phonelist);
				contacts.push({label:contact.name, value:contact.phones[0]});
			}
		});
	};
	var authorize = function() {
		if(isAuthorized) return true;
		oauthChannel.message({command:'authorize'}, function(reply) {
			//console.log("oauthoeruoeuoru repply: %o", reply);
			if(reply.result == "authorized") {
				isAuthorized = true;
			} else {
				window.setTimeout(authorize, 10*1000);
			}
			
		});		
	};
	var input = $("#cellular").autocomplete({
		source: function( request, response ) {
			console.log("searching: %o", request);
			console.log(contacts);
			var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
			response( $.grep( contacts, function( item ){
				return matcher.test( item.label );
			}) );
		}
		});
	authorize();
	getContacts();
}

window.setTimeout(main, 1000);
