﻿
// NRK+SMS

// scaffolding code from chrome extension example oauth_contacts, with original copyright header:
// Copyright (c) 2012 The Chromium Authors. All rights reserved.

var oauth = ChromeExOAuth.initBackgroundPage({
		'request_url': 'https://www.google.com/accounts/OAuthGetRequestToken',
		'authorize_url': 'https://www.google.com/accounts/OAuthAuthorizeToken',
		'access_url': 'https://www.google.com/accounts/OAuthGetAccessToken',
		'consumer_key': 'anonymous',
		'consumer_secret': 'anonymous',
		'scope': 'https://www.google.com/m8/feeds',
		'app_name': 'NRK SMS + Google contacts'
	});

var contacts = null;

function setIcon() {
	if (oauth.hasToken()) {
		chrome.browserAction.setIcon({ 'path' : 'img/icon-19-on.png'});
	} else {
		chrome.browserAction.setIcon({ 'path' : 'img/icon-19-off.png'});
	}
};

function onContacts(text, xhr) {
	contacts = [];
	var data = JSON.parse(text);
	for (var i = 0, entry; entry = data.feed.entry[i]; i++) {
		console.log(entry);
		var contact = {
			'name' : entry['title']['$t'],
			'id' : entry['id']['$t'],
			'emails' : [],
			'phones' : []
		};

		if (entry['gd$email']) {
			var emails = entry['gd$email'];
			for (var j = 0, email; email = emails[j]; j++) {
				contact['emails'].push(email['address']);
			}
		}

		if (entry['gd$phoneNumber']) {
			var phones = entry['gd$phoneNumber'];
			for (var j = 0, phone; phone = phones[j]; j++) {
				contact['phones'].push(phone['$t']);
			}
		}

		if (!contact['name']) {
			contact['name'] = contact['emails'][0] || "<Unknown>";
		}
		BackgroundChannel.push('oauth', {command:'getContacts', result: contact});
		contacts.push(contact);
	}
	//chrome.tabs.create({ 'url' : 'contacts.html'});
	console.log(contacts);
};

function getContacts() {
	oauth.authorize(function() {
		console.log("on authorize");
		var url = "https://www.google.com/m8/feeds/contacts/default/full";
		oauth.sendSignedRequest(url, onContacts, {
			'parameters' : {
				'alt' : 'json',
				'v': '3.0',
				'max-results' : 100
			}
		});
	});
};

function logout() {
	oauth.clearTokens();
	//setIcon();
};

var BackgroundChannel = {
	_ports : [],
	_receivers : {},
	setup : function() {
		chrome.runtime.onConnect.addListener(BackgroundChannel._incoming);
	},
	_incoming : function(port) {
		console.log("incoming connection port : %o", port);
		port.onMessage.addListener(function(msg) { BackgroundChannel._receive(port, msg) });
		BackgroundChannel._ports.push(port);
	},
	_receive : function(port, msg) {
		console.log("backgrond.js receive: %o", msg);
		var receiver = BackgroundChannel._receivers[port.name];
		var reply = receiver(msg);
		port.postMessage(reply);
	},
	addReceiver : function(channelname, callback) {
		BackgroundChannel._receivers[channelname] = callback;
	},
	findPort : function(channelname) {
		for(var i = 0; i<BackgroundChannel._ports.length; i++) {
			if(BackgroundChannel._ports[i].name == channelname)
				return BackgroundChannel._ports[i];
		}
		return false;
	},
	push : function(channelname, msg) {
		var port = BackgroundChannel.findPort(channelname);
		port.postMessage(msg);
	}
}

BackgroundChannel.setup();
authorizing = false;

function receiveOauth(msg) {
	console.log("receiveOauth: %o", msg);
	switch(msg.command) {
		case "authorize":
			var t = oauth.getToken();
			if(t) {
				authorizing = false;
				return {command:'authorize', result:'authorized'}
			} else {
				if(authorizing) {
					// oauth auth already started
				} else {
					authorizing = true;
					oauth.authorize(function() { console.log("authorized"); authorizing = false});
				}
				return {command:'authorize', result:'authorizing'};
			}
			break;
		case "signURL":
			return {command:'signedurl', result:oauth.signURL(msg.url, msg.method, msg.params)};
			break;
		case "getContacts":
			if(authorizing) {
				return {command:'getContacts', result:false};
			} else {
				getContacts();
				return {command:'getContacts', result:true};
			}
			break;
	}
}

BackgroundChannel.addReceiver("oauth", receiveOauth);
/*

// This event is fired each time the user updates the text in the omnibox,
// as long as the extension's keyword mode is still active.
chrome.omnibox.onInputChanged.addListener(
  function(text, suggest) {
    //console.log('inputChanged: ' + text);
    suggest([
      {content: "Etternavn::"+text, description: "Søk på etternavn"},
      {content: "Fornavn::"+text, description: "Søk på fornavn"},
      {content: "Telefon::"+text, description: "Søk på telefonnummer"}
    ]);
  });

// This event is fired with the user accepts the input in the omnibox.
chrome.omnibox.onInputEntered.addListener(
  function(text) {
		var input = text.split("::");
		var mode = 'Fritekst';
		try {
			mode = input[0];
			var query = input[1];
		} catch(e) {
			var query = input[0];
		}
    console.log("Looking for %o (%o)", query, mode);
		chrome.tabs.create({url: 'http://maweb01/telesok/resultat.asp?'+mode+'='+escape(query)});
  });
*/
