/* nrkatalog_sms 

requires jquery

havard.gulldahl@nrk.no, 2011

*/


$(function() {
console.log("nrkatalog+sms running...");

$.ajaxSetup({
			contentType: "application/x-www-form-urlencoded; charset=utf-8",
			error: function(jqxhr, text, ex) {
				console.log("ajax failed: %o -> %o", text, ex);
			}
});

function sms(number, message) {
//http://online.telefonkatalogen.no/online/sendsms.php?cellular=99710615&emailaddr=havard.gulldahl%40nrk.no&havecheckbox=1&imageField2.x=40&imageField2.y=14&msg=hei-1&sender=99710615&sendsmsemail=smsconfirm

				jQuery.post("http://online.telefonkatalogen.no/online/sendsms.php",
						{'cellular': number,
						 'emailaddr' : 'havard.gulldahl@nrk.no',
						 'havecheckbox' : 1,
						 'msg': message,
						 'sendsmsemail': 'smsconfirm'},
						function(data) {
							console.log("got telefonkat-data: %o", data);
							$("#smsbox").remove();
						});

}

var hits = $("body>table>tbody tr");

jQuery.each(hits, function(idx, el) {
	try {
		var cell = $(el).find("td:nth-child(10)");
		// console.log("got cell: %o", cell);
		var t = cell.text();
		if(t.length < 8) {
			return true; // continue
		};
		cell.text("");
		$("<a>")
			.attr("id", "tlf-"+t)
		  .attr("href", "#tlf-"+t)
			.attr("title", "Send sms")
			.text(t)
			.data("phoneno", t)
			.appendTo(cell)
			.click(function() {
				var no = $(this).data("phoneno");
				console.log("smsing %o", no);
				var b = $("<div id=smsbox style='position:absolute; top:50%;left:50%; background:white;padding:2em;border:1px solid black;border-radius:10px;'><h5>Tekstmelding til "+no+"</h5><textarea id=smstext placeholder='Skriv meldingen her'></textarea>")
					.appendTo("body");
				$("<button>")
					.text("Send")
					.click(function() {
						var msg = $("#smstext").val();
						console.log("sending.. %o", msg);
						sms(no, msg);
					})
					.appendTo(b);
					
			});
	} catch(e) {
		console.log(e);
		return true;
	}
});


});